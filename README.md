TigerOS Build Scripts
=====================

## Instructions

### Setup 
* Clone this repo
```  
  git clone https://gitlab.com/RITlug/TigerOS/build-scripts.git
```

### Build the ISO
From the directory where you cloned this repo run the commands below. You may need to preface it with `sudo` if you do not have root privileges.
```
chmod +x make-iso.sh
./make-iso.sh
```
The build process should take approximately 40 minutes to complete. The initial build time will vary based on specs. 

## Current Team
* Tim Zabel (Scripts and UI) <tjz8659@rit.edu>
* Nathaniel Larrimore (Scripts) <nlmeminger@gmail.com>
* Christian Martin (Infra) (find me on IRC as ctmartin)


### Honorable Mentions
* Aidan Kahrs (Former Project Lead, Consulting) <axk4545@rit.edu>
* Josh Bicking (Tutorials and Scripts) <jhb2345@rit.edu>
* Regina Locicero (Designer) <rtl3971@rit.edu>

*Additional thanks to [many others](https://github.com/RITlug/TigerOS/graphs/contributors) for their contributions.*

## Contributing
Please see [CONTRIBUTING.md](CONTRIBUTING.md)

## Resources
* [RITlug website](http://ritlug.com)
* [RITlug GitHub](https://github.com/RITlug)
* [Building Fedora Remixes](https://docs.fedoraproject.org/en-US/remix-building/remix-ci/)
* [About Fedora Remixes](https://fedoraproject.org/wiki/Remix)

**Join the conversation! Find us on [#rit-tigeros](https://webchat.freenode.net/?channels=rit-tigeros) on the [Freenode IRC network](https://freenode.net/) for development discussions and support.**
